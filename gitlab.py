import requests
import json
from statistics import mean


# GitLab API base URL
base_url = "https://gitlab.example.com/api/v4/" # update this

# Your GitLab access token
access_token = "your token here" #give read api access

# Function to fetch project data and job runs
def fetch_project_data():
    projects_url = f"{base_url}projects"
    headers = {"Private-Token": access_token}
    params = {"per_page": 100}  # Increase per_page to fetch more projects if needed
    projects = []

    while True:
        response = requests.get(projects_url, headers=headers, params=params)
        if response.status_code == 200:
            projects.extend(response.json())
            if 'next' in response.links:
                projects_url = response.links['next']['url']
            else:
                break
        else:
            print("Failed to retrieve projects.")
            break

    # Fetch job runs for each project
    project_data = []
    for project in projects:
        job_runs = get_recent_job_runs_sorted(project["id"])
        if len(job_runs) > 0:
            job_names = set()  # Store distinct job names for this project
            job_durations = {}  # Store total durations for each distinct job
            for job in job_runs:
                job_name = job["name"]
                if job_name not in job_names:
                    job_names.add(job_name)
                    job_durations[job_name] = [job["duration"]]
                else:
                    job_durations[job_name].append(job["duration"])
            
            # Calculate average time for each distinct job
            for job_name, durations in job_durations.items():
                average_time = calculate_average_time(durations)
                project_data.append({"project_name": project["name"], "job_name": job_name, "average_time": average_time})

    # Write project data to a JSON file
    with open("project_data.json", "w") as data_file:
        json.dump(project_data, data_file)

    return project_data

# Get recent job runs for a project and sort by duration, limit to 10 pages or less
def get_recent_job_runs_sorted(project_id):
    job_runs_url = f"{base_url}projects/{project_id}/jobs"
    headers = {"Private-Token": access_token}
    params = {"scope": "success", "per_page": 30, "order_by": "duration", "sort": "desc"}
    job_runs = []

    for page in range(1, 30):  # Limit to a maximum of 10 pages
        params["page"] = page
        response = requests.get(job_runs_url, headers=headers, params=params)
        if response.status_code == 200:
            page_data = response.json()
            if len(page_data) > 0:
                job_runs.extend(page_data)
            else:
                # No more data available, break out of the loop
                break
        else:
            print(f"Failed to retrieve job runs for project {project_id}.")
            break

    return job_runs


# Calculate average time for recent job runs
def calculate_average_time(durations):
    return f"{round(mean(durations) / 60, 2)} min"

# Generate HTML report
def generate_html_report(projects):
    with open("index.html", "w") as html_file:
        html_file.write("<html><head><title>Job Report</title></head><body>")
        html_file.write("<table border='1'><tr><th>Job Name</th><th>Project Name</th><th>Average Time (last 30 runs)</th></tr>")
        
        # Sort the project data by average_time in descending order
        projects.sort(key=lambda x: float(x["average_time"].split()[0]), reverse=True)

        # Write the sorted project data to the HTML file
        for data in projects:
            html_file.write(f"<tr><td>{data['job_name']}</td><td>{data['project_name']}</td><td>{data['average_time']}</td></tr>")
        
        html_file.write("</table></body></html>")

if __name__ == "__main__":
    try:
        # Attempt to load project data from the JSON file
        with open("project_data.json", "r") as data_file:
            project_data = json.load(data_file)
    except FileNotFoundError:
        # If the file is not found, fetch the project data
        project_data = fetch_project_data()

    generate_html_report(project_data)
